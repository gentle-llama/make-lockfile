#!/usr/bin/env python3

import argparse
import gzip
import json
import logging
import os
import xml.etree.ElementTree as ET
import requests

# Module constants
# Distros
DISTROS_CS = ["cs9"]
DISTROS_RHEL = ["rhel9"]
DISTROS = DISTROS_CS + DISTROS_RHEL
DEFAULT_DISTRO = DISTROS_CS[0]

# Default Repos
DEFAULT_REPOS = {
    "rhel9": [],
    "cs9": [
        "https://buildlogs.centos.org/9-stream/automotive/aarch64/packages-main",
        "https://buildlogs.centos.org/9-stream/automotive/x86_64/packages-main",
        "https://composes.stream.centos.org/production/latest-CentOS-Stream/compose/BaseOS/aarch64/os",
        "https://composes.stream.centos.org/production/latest-CentOS-Stream/compose/BaseOS/x86_64/os",
        "https://composes.stream.centos.org/production/latest-CentOS-Stream/compose/AppStream/aarch64/os",
        "https://composes.stream.centos.org/production/latest-CentOS-Stream/compose/AppStream/x86_64/os",
        "https://composes.stream.centos.org/production/latest-CentOS-Stream/compose/CRB/aarch64/os",
        "https://composes.stream.centos.org/production/latest-CentOS-Stream/compose/CRB/x86_64/os",
    ],
}

# Default content-resolver inputs
DEFAULT_ENVS = [
    "https://tiny.distro.builders/workload--automotive-workload-in--automotive-environment--automotive-repositories-c9s--aarch64.json",
    "https://tiny.distro.builders/workload--automotive-workload-in--automotive-environment--automotive-repositories-c9s--x86_64.json",
    "https://tiny.distro.builders/workload--automotive-workload-off--automotive-environment--automotive-repositories-c9s--aarch64.json",
    "https://tiny.distro.builders/workload--automotive-workload-off--automotive-environment--automotive-repositories-c9s--x86_64.json",
]

# Hardcoded corner cases needed when converting the product definition
# (defined using the Centos Stream tree) into a Red Hat tree
CENTOS_TO_RHEL_PACKAGE_MAP = {
    "centos-gpg-keys": [],
    "centos-logos": ["redhat-logos"],
    "centos-stream-release": ["redhat-release"],
    "centos-stream-repos": [],
    "dnf": ["dnf", "librhsm"],
    "kernel-automotive": ["kernel"],
    "kernel-automotive-core": ["kernel-core"],
    "kernel-automotive-modules": ["kernel-modules"],
    "realtime-setup": [],
    "tuned-profiles-realtime": [],
}


def get_package_names_from_json(crjson, distro):
    """Extracts set of package names from a content-resolver json output"""
    packages = set()
    for pkg in crjson["pkg_query"]:
        pkg_name = pkg["name"]
        # Hardcoded corner cases for handling RHEL names
        if distro in DISTROS_RHEL and pkg_name in CENTOS_TO_RHEL_PACKAGE_MAP:
            pkg_name = CENTOS_TO_RHEL_PACKAGE_MAP.get(pkg_name)
            # The hardcoded corner cases may return a list of replacement packages
            # (many to one mapping) so use update func to support that
            packages |= set(pkg_name)
        else:
            packages.add(pkg_name)
    return packages


def get_nvrs(repos):
    """Resolve list of nvrs from a set of repos"""
    nvrs = {}
    for repo in repos:
        # Avoid pulling lots of deps by just parsing the xml directly
        # Find primary.xml via repomd.xml
        logging.debug("Fetching repomd: %s/repodata/repomd.xml", repo)
        resp = requests.get(f"{repo}/repodata/repomd.xml")
        repomd = ET.fromstring(resp.content)
        ns = "{http://linux.duke.edu/metadata/repo}"
        primary_data = repomd.find(f"{ns}data[@type='primary']/{ns}location")
        primary_xml_path = primary_data.attrib["href"]
        logging.debug("Primary xml: %s", primary_xml_path)

        # Fetch primary.xml and parse package tags
        logging.debug("Handling primary: %s/%s", repo, primary_xml_path)
        resp = requests.get(f"{repo}/{primary_xml_path}")
        primary_xml = gzip.decompress(resp.content)
        ns = "{http://linux.duke.edu/metadata/common}"
        primary = ET.fromstring(primary_xml)

        # Iterate through the package elements
        # If there are multiple versions of a package, they're luckily sorted
        # from oldest to newest so the final nevra is the newest
        for package in primary:
            if package.tag != f"{ns}package":
                continue
            name = package.find(f"./{ns}name")
            full_version = package.find(f"./{ns}version")
            ver = full_version.attrib["ver"]
            rel = full_version.attrib["rel"]

            nvrs[name.text] = f"{ver}-{rel}"

        logging.debug("Resolved: %d packages", len(primary))

    return nvrs


def get_envfile(crjson_env):
    if crjson_env.startswith("http"):
        logging.debug("Downloading: %s", crjson_env)
        resp = requests.get(crjson_env)
        logging.debug("Downloaded: %s", resp.url)
        env_json = resp.json()
    else:
        logging.debug("Loading: %s", crjson_env)
        with open(crjson_env) as json_file:
            env_json = json.load(json_file)
        logging.debug("Loaded.")

    return env_json


def merge_packages_names_and_nvrs(package_names, nvrs):
    # Resolve nvrs + package_names together and return as a set
    packages = set()
    unresolvable_packages = set()
    for package_name in package_names:
        if package_name in nvrs:
            packages.add(f"{package_name}-{nvrs[package_name]}")
        else:
            unresolvable_packages.add(package_name)

    if unresolvable_packages:
        raise Exception(f"No nevra's available for packages: {unresolvable_packages}")

    return packages


def create_lockfile(distro, packages_aarch64, packages_x86):
    # Determine the common and architecture unique packages using set maths
    common = packages_x86.intersection(packages_aarch64)
    x86 = packages_x86.difference(common)
    aarch64 = packages_aarch64.difference(common)

    # Generate the package manifests json file for automotive-sig
    # See: https://gitlab.com/redhat/automotive/automotive-sig/-/blob/main/package_list/cs9-image-manifest.lock.json
    output = {
        distro: {
            "common": sorted(list(common), key=str.lower),
            "arch": {
                "aarch64": sorted(list(aarch64), key=str.lower),
                "x86_64": sorted(list(x86), key=str.lower),
            },
        }
    }

    return output


def main():
    parser = argparse.ArgumentParser(
        description="Convert tiny.distro.builders jsons to lockfile json"
    )
    parser.add_argument("--distro", type=str, default=DEFAULT_DISTRO, choices=DISTROS)
    parser.add_argument(
        "--env",
        type=str,
        nargs="+",
        default=DEFAULT_ENVS,
        help="URLs or paths to content-resolver environment jsons",
    )
    parser.add_argument(
        "--repos", type=str, nargs="+", help="Yum repos to resolve nvr's from"
    )
    parser.add_argument(
        "--debug", action="store_true", help="Print debug messages to stderr"
    )
    parser.add_argument(
        "--outfile", nargs="?", type=str, help="Output file in json format"
    )
    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)

    # Set some default env json urls and/or repos if none are manually set
    crjson_envs = args.env  # default provided by argparse through default kwarg
    repos = args.repos or DEFAULT_REPOS[args.distro]

    # Sanity check that repos is not empty
    if not repos:
        raise Exception(
            f"No default repositories defined for distro {args.distro}! You can set repositories with the --repos param."
        )

    # Get nvrs from repos
    logging.debug("Grabbing nvrs from repos: %s", repos)
    nvrs = get_nvrs(repos)
    logging.debug("%d nvrs resolved.", len(nvrs))

    # Initiate sets of packages
    packages_aarch64 = set()
    packages_x86 = set()

    # Parse env json files from content-resolver
    # Download if url otherwise open from local filesystem
    for crjson_env in crjson_envs:
        # Get env json from content-resolver
        env_json = get_envfile(crjson_env)

        # Parse env json
        logging.debug("Parsing env json.")
        package_names = get_package_names_from_json(env_json, args.distro)
        logging.debug("%d packages in environment.", len(package_names))

        # Update package sets with packages from content-resolver
        packages = merge_packages_names_and_nvrs(package_names, nvrs)
        env_file = os.path.basename(crjson_env)
        if "aarch64" in env_file:
            packages_aarch64 |= packages
        elif "x86_64" in env_file:
            packages_x86 |= packages
        else:
            raise Exception(
                f"Invalid package architecture for environment file: {env_file}"
            )

    # Generate lockfile output from sets of packages
    output = create_lockfile(args.distro, packages_aarch64, packages_x86)

    if args.outfile:
        with open(args.outfile, "w", encoding="utf-8") as f:
            json.dump(output, f, indent=4)
    else:
        print(json.dumps(output, indent=4))


if __name__ == "__main__":
    main()
