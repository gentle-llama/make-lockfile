import gzip
import importlib
import json
import random
import re
from pathlib import Path

import pytest
import requests_mock
from hamcrest import assert_that, equal_to

# Since our script has a dash, we have to use importlib to import it
makelockfile = importlib.import_module("make-lockfile")

TEST_DIR = Path(__file__).parent

HEX_DIGIT_REGEX = "[0-9a-fA-F]"

PRIMARY_XML_STEM_REGEX = f"(?P<path>[\\w/]*)/{HEX_DIGIT_REGEX}{{64}}-primary\\.xml\\.gz"
REPOMD_XML_STEM_REGEX = "(?P<path>[\\w/]*)/repomd\\.xml"


def repomd_xml(distro):
    test_repomd = TEST_DIR / "data" / f"test-{distro}-x86_64-baseos-repomd.xml"
    return test_repomd.read_bytes()


def primary_xml(distro):
    test_primary = TEST_DIR / "data" / f"test-{distro}-x86_64-baseos-primary.xml"
    return gzip.compress(test_primary.read_bytes())


def content_tree_json():
    test_content_file = TEST_DIR / "data" / "test-content-tree.json"
    return test_content_file.read_bytes()


@pytest.fixture
def random_repo(request):
    distro = request.param
    choices = makelockfile.DEFAULT_REPOS[distro]
    choice = random.choice(choices)
    return choice, distro


# Tests
@pytest.mark.parametrize("distro", ["cs9", "rhel9"])
def test_get_package_names_from_json(distro):
    test_content_file = TEST_DIR / "data" / "test-content-tree.json"
    with test_content_file.open("r") as json_file:
        test_content_tree = json.load(json_file)

    expected_names_file = TEST_DIR / "data" / f"expected-{distro}-package-names-x86_64.json"
    with expected_names_file.open("r") as json_file:
        expected_names = json.load(json_file)

    actual_package_names = makelockfile.get_package_names_from_json(test_content_tree, distro)
    assert actual_package_names == set(expected_names)


# There are only repos defined for cs9
@pytest.mark.parametrize("random_repo", ["cs9"], indirect=True)
def test_get_nvrs(random_repo):
    repo, distro = random_repo
    with requests_mock.Mocker() as mock:
        for xml_file_type in ("repomd", "primary"):
            if xml_file_type == "repomd":
                get_xml_method = repomd_xml
                stem_regex = REPOMD_XML_STEM_REGEX
            else:
                get_xml_method = primary_xml
                stem_regex = PRIMARY_XML_STEM_REGEX

            uri_regex = re.compile(f"{re.escape(repo)}/{stem_regex}")

            mock.register_uri(
                "GET",
                uri_regex,
                status_code=200,
                content=get_xml_method(distro)
            )

        expected_nvrs_file = TEST_DIR / "data" / f"expected-{distro}-nvrs.json"
        with expected_nvrs_file.open("r") as json_file:
            expected_nvrs = json.load(json_file)

        # use only a single, randomly chosen repo to shorten test duration
        # the test is setup such that all repos have their XML responses mocked
        # to what is in our test files, so iterating over all of them really just
        # tests the same thing len(repos) times
        actual_nvrs = makelockfile.get_nvrs([repo])
        assert_that(actual_nvrs, equal_to(expected_nvrs))


def test_get_envfile_http():
    with requests_mock.Mocker() as mock:
        mock.register_uri("GET", "http://test.json", status_code=200, content=content_tree_json())
        env_json = makelockfile.get_envfile("http://test.json")

    test_content_file = TEST_DIR / "data" / "test-content-tree.json"
    with test_content_file.open("r") as json_file:
        test_content_tree = json.load(json_file)

    assert (env_json == test_content_tree)


def test_get_envfile_local():
    test_content_file = TEST_DIR / "data" / "test-content-tree.json"
    env_json = makelockfile.get_envfile(str(test_content_file))

    with test_content_file.open("r") as json_file:
        test_content_tree = json.load(json_file)
    assert (env_json == test_content_tree)


def test_merge_packages_names_and_nvrs():
    vr_a = "10.8-1"
    vr_b = "100-0"
    vr_c = "a-a"

    nvrs = {"a": vr_a, "b": vr_b, "c": vr_c}

    test_set = makelockfile.merge_packages_names_and_nvrs({"a", "b"}, nvrs)
    assert ({f"a-{vr_a}", f"b-{vr_b}"} == test_set)

    test_set = makelockfile.merge_packages_names_and_nvrs({"c"}, nvrs)
    assert ({f"c-{vr_c}"} == test_set)


@pytest.mark.parametrize("distro", ["cs9", "rhel9"])
def test_create_lockfile(distro):
    nvr_a = "a-10.8-1"
    nvr_b = "b-100-0"
    nvr_c = "c-a-a"
    nvr_d = "d-b-b"

    packages_aarch64 = {nvr_a, nvr_b, nvr_c}
    packages_x86 = {nvr_a, nvr_b, nvr_d}
    output = makelockfile.create_lockfile(distro, packages_aarch64, packages_x86)

    expected_output = {
        distro: {
            "common": [
                nvr_a,
                nvr_b,
            ],
            "arch": {
                "aarch64": [
                    nvr_c,
                ],
                "x86_64": [
                    nvr_d,
                ],
            }
        }
    }
    assert (expected_output == output)
